# The Rolling Pet Adventure Game - Unity

## Models

### Main Character (Toon Fox)


![fox](images/1.png)
![fox](images/2.png)
![fox](images/3.png)

 #### The fox can turn into a ball when it takes the converter coin 

![alt](images/4.png)
![alt](images/5.png)


#### Animations

![alt](images/6.png)



### Road Creator


[Bézier Path Creator | Utilities Tools | Unity Asset Store]([https://](https://assetstore.unity.com/packages/tools/utilities/b-zier-path-creator-136082))

![alt](images/7.png)
![alt](images/8.png)
![alt](images/9.png)
![alt](images/10.png)


## Game Elements

### Coin

There are 3 types of coin sets: 4 coins - 6 coins - 4 coins Square

![alt](images/12.png)
![alt](images/11.png)
![alt](images/13.png)
![alt](images/14.png)

## Arena

Each level consists of at least one arena.

They are connected by slips or ground blocks.


![alt](images/15.png)
![alt](images/16.png)


## Blocks

### Green Blocks

![alt](images/17.png)

### Breakable Block

![alt](images/18.png)

### Walls

![alt](images/19.png)
 

### Converter Coins

![alt](images/20.png)

### Mines

![alt](images/21.png)


### Gems

![alt](images/22.png)


### Winning Stage

![alt](images/23.png)


### Gates

![alt](images/24.png)

### Power Up (Shield)

![alt](images/25.png)

### Connectors

![alt](images/26.png)
![alt](images/27.png)

### Volleyballs

![alt](images/28.png)
![alt](images/29.png)



## Game Play

### Controls

![alt](images/29-1.png)


The user can move the character forward and backward using arrows or W and S keys.
Also, we can rotate the character left and right using left and right arrows or A and D keys.


The character will move faster when we press the left shift key, while we are moving in a certain direction.


The camera will follow the moving character unless we press the right mouse button, this will make us able to rotate the camera around the character.

The fox can attack the nearby breakable blocks when we press the F key.
The block will break after 4 attacks.

![alt](images/30.png)

![alt](images/31.png)




Collectible Items
The player can collect coins, power-ups, and gems.

![alt](images/32.png)


The character can have a shield when it takes the power-up items.

![alt](images/33.png)

![alt](images/34.png)


The fox will turn into a ball that is moved by rolling.

![alt](images/35.png)

The ball will turn back to a fox when it takes another converter coin

![alt](images/36.png)

![alt](images/37.png)


The fox will lose its shield if it collides with one of the mines

![alt](images/38.png)


The fox will die if it collides with a mine with no shield

![alt](images/39.png)


![alt](images/40.png)
![alt](images/41.png)
![alt](images/42.png)
![alt](images/43.png)
![alt](images/44.png)
![alt](images/45.png)


## Game Start Scene

![alt](images/46.png)



### Level 1

![alt](images/47.png)
![alt](images/48.png)

### Level 2
![alt](images/49.png)
![alt](images/50.png)
![alt](images/51.png)
![alt](images/52.png)
![alt](images/53.png)


# Contact With Me

## Rasoul Abouassaf - AI Engineer

* [Rasoul Abouassaf- Linked-In](https://www.linkedin.com/in/rasoul-abouassaf-149005172/)

* [rasoul.19.assaf - GitLab](https://www.linkedin.com/in/rasoul-abouassaf-149005172/)

* Email: rasoul.a.assaf@gmail.com
* [My Portfolio](https://www.canva.com/design/DAFNzXHIg5g/d11mR0vBrQUpW-wvBum-kA/edit?utm_content=DAFNzXHIg5g&utm_campaign=designshare&utm_medium=link2&utm_source=sharebutton) 